export function loginSuccess (state, user) {
  state.isLogged = true
  state.user = user.user
}
export function loginFailure (state, user) {
  state.loggedIn = false
  state.user = null
}
export function logout (state, user) {
  state.loggedIn = false
  state.user = null
}
