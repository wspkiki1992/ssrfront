
import { apiSubmit } from '@api/volunteer'

export async function submit ({ commit }, postData) {
  try {
    const resData = await this.$axios({ ...apiSubmit(), data: postData })
    return resData.data
  } catch (error) {
    return error
  }
}
