import { Cookies } from 'quasar'
export const setCookies = (key, value) => {
  Cookies.set(key, value)
}

export const getCookies = (key, ssrContext) => {
  const cookies = process.env.SERVER
    ? Cookies.parseSSR(ssrContext)
    : Cookies
  return cookies.get(key)
}
