// This is just an example,
// so you can safely delete all default props below

export default {
  failed: 'Action failed',
  success: 'Action was successful',
  layout: {
    menuList: {
      aboutUs: 'ABOUT US',
      home: 'Home',
      contactUs: 'CONTACT US',
      animals: 'Animals',
      volunteer: 'Volunteer'
    },
    login: 'Login',
    logout: 'Logout'
  },
  animals: {
    status: {
      0: '待領養',
      1: '觀察中',
      2: '已領養'
    },
    type: {
      1: '狗'
    }

  }
}
