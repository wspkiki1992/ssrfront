export default function (context) {
  const routes = [
    {
      path: '/',
      component: () => import('layouts/MainLayout.vue'),
      children: [
        { name: 'home', path: '', component: () => import('pages/home/index.vue') },
        {
          name: 'login',
          path: 'login',
          component: () => import('pages/login/index.vue')
        },
        {
          name: 'profile',
          path: 'profile',
          component: () => import('pages/profile/index.vue'),
          meta: { requiresAuth: true }
        },
        {
          name: 'aboutUs',
          path: 'aboutUs',
          component: () => import('pages/aboutUs/index.vue')
        },
        {
          name: 'animals',
          path: 'animals',
          component: () => import('pages/animals/index.vue')
        },
        {
          name: 'animalDetail',
          path: 'animal/:id',
          component: () => import('pages/animals/detail.vue')
        },
        {
          name: 'volunteer',
          path: 'volunteer',
          component: () => import('pages/volunteer/index.vue')
        }
      ],
      beforeEnter (to, from, next) {
        if (to.matched.some(record => record.meta.requiresAuth)) {
          const { store } = context
          if (!store.state.auth.isLogged) {
            next({
              path: '/login'
            })
          } else {
            next()
          }
        } else {
          next()
        }
      }
    }
  ]
  // Always leave this as last one
  if (process.env.MODE !== 'ssr') {
    routes.push({
      path: '*',
      component: () => import('pages/Error404.vue')
    })
  }
  return routes
}
