export function apiGetAnimalList () {
  return {
    url: 'api/animal/list',
    method: 'get'
  }
}
export function apiGetAnimal () {
  return {
    url: 'api/animal/one',
    method: 'get'
  }
}
