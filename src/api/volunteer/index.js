export function apiSubmit () {
  return {
    url: 'api/volunteer/submit',
    method: 'post'
  }
}
