import { getCookies } from '@utils/cookies'

export default function ({ app, store, router, ssrContext }) {
  const token = getCookies('token', ssrContext)
  const user = getCookies('user', ssrContext)
  if (token) {
    store.commit('auth/loginSuccess', { user: user })
  }
}
