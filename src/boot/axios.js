import Vue from 'vue'
import axios from 'axios'

import { Cookies } from 'quasar'

Vue.mixin({
  beforeCreate () {
    const options = this.$options
    if (options.axios) {
      this.$axios = options.axios
    } else if (options.parent) {
      this.$axios = options.parent.$axios
    }
  }
})

export default function ({ app, store, router, ssrContext }) {
  const instance = axios.create({
    baseURL: 'https://fronttest0.ylnet.net/'
    // baseURL: process.env.DEV ? process.env.DEV_API_URL : process.env.PROD_API_URL
  })
  const cookies = process.env.SERVER
    ? Cookies.parseSSR(ssrContext)
    : Cookies

  instance.interceptors.request.use(config => {
    const token = cookies.get('token')
    if (token) {
      config.headers.Authorization = `bearer ${token}`
    }
    return config
  }, error => {
    return Promise.reject(error)
  })

  app.axios = instance
  store.$axios = instance
  router.$axios = instance
}
