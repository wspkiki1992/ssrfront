# Front-end

## 🐕Linmom Doggy Shelter🐕

---

![readme/Untitled.png](readme/Untitled.png)

[View Demo](https://fronttest0.ylnet.net/)

## Tech Stack

---

![readme/techStack.png](readme/techStack.png)

## Motivation

---

- 以往製作的專案皆無SEO需求，因此大多為SPA為主，因此透過此次製作SSR專案
- 由於狗園經費有限，希望透過自身專業幫忙製作網站或協助讓更多人認識浪浪，進而提高認養率
